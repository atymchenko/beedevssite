<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 10/1/2016
 * Time: 9:04 PM
 */

return [
    'contact' => ['url' => 'contact'],
    'about_us' => ['url' => 'about-us'],
    'services' => ['url' => 'services'],
    'ecommerce_site' => ['url' => 'ecommerce-site'],
    'custom_software' => ['url' => 'custom-software'],
    'it_consulting' => ['url' => 'it-consulting'],
    'graphic_design' => ['url' => 'graphic-design'],
    'online_entertainment' => ['url' => 'online-entertainment'],
    'project_management' => ['url' => 'project-management'],
    'quality_assurance' => ['url' => 'quality-assurance'],
    'website_project/$1' => ['url' => 'website-project/$1', 'params' => ['([-_a-z0-9]?)']],
    'website/$1' => ['url' => 'website/$1', 'params' => ['([0-9]?)']],
    'game_studio' => ['url' => 'game-studio'],
    'submit' => ['url' => 'submit'],
    'index' => ['url' => '']
];