<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 10/2/2016
 * Time: 1:27 AM
 */
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

function index() {
    return new Response(
        twig()->render('index.html.twig')
    );
}

function about_us() {
    return new Response(
        twig()->render('about-us.html.twig')
    );
}

function services() {
    return new Response(
        twig()->render('services.html.twig')
    );
}

function contact() {
    return new Response(
        twig()->render('contact.html.twig')
    );
}

function custom_software() {
    return new Response(
        twig()->render('custom-software.html.twig')
    );
}

function ecommerce_site() {
    return new Response(
        twig()->render('ecommerce-site.html.twig')
    );
}

function it_consulting() {
    return new Response(
        twig()->render('it-consulting.html.twig')
    );
}

function graphic_design() {
    return new Response(
        twig()->render('graphic-design.html.twig')
    );
}

function online_entertainment() {
    return new Response(
        twig()->render('online-entertainment.html.twig')
    );
}

function project_management() {
    return new Response(
        twig()->render('project-management.html.twig')
    );
}

function quality_assurance() {
    return new Response(
        twig()->render('quality-assurance.html.twig')
    );
}

function website($page = 1) {
    $per_page = 4;

    if (empty($page)) {
        $page = 1;
    }

    try {
        $res = pdo()->query("SELECT * FROM `websites`;");
        if ($res === false) {
            return new Response("unable to select from DB", Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $projects = array_slice($res->fetchAll(), $page * $per_page - $per_page, $per_page);
    } catch (PDOException $e) {
        return new Response("unable to select from DB", Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return new Response(
        twig()->render('website.html.twig', array(
            'page_num' => $page,
            'pages_total' => ceil($res->rowCount()/$per_page),
            'projects' => $projects
        ))
    );
}

function website_project($name) {

    try {
        $res = pdo()->query("SELECT * FROM `websites`;");
        if ($res === false) {
            return new Response("unable to select from DB", Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        foreach($res as $p) {
            if ($p['name_canonical'] == $name) {
                $project = $p;
                break;
            }
        }

    } catch (PDOException $e) {
        return new Response("unable to select from DB", Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    $gallery = array_diff(scandir('images/webproj/gallery/'.$project['id']), array('.', '..'));
    foreach ($gallery as &$image) {
        $image = 'images/webproj/gallery/'.$project['id'].'/'.$image;
    }

    return new Response(
        twig()->render('website-project.html.twig', array(
            'project' => $project,
            'gallery' => $gallery
        ))
    );
}

function submit() {
    $request = Request::createFromGlobals();
    if( !($request->request->has('name') && $request->request->has('email') && $request->request->has('text')) ) {
        return new Response("Parameters 'name', 'email', 'text' are required.", Response::HTTP_UNPROCESSABLE_ENTITY);
    }
    $name = $request->request->get('name');
    $email = $request->request->get('email');
    $text = $request->request->get('text');

    mail('team@beedevs.com', 'beeDEVS Contact Request', 'NAME: '.$name.'. EMAIL: '.$email.' TEXT: '.$text, 'From: beeDEVS <info@beedevs.com>' . "\r\n");

    try {
        $ps = pdo()->prepare("INSERT INTO `contact` (`name`, `email`, `text`) VALUES (:name, :email, :text);");
        if ($ps === false) {
            return new Response("unable to save to DB", Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        if ($ps->execute(array(':name' => $name, ':email' => $email, ':text' => $text)) === false) {
            return new Response("unable to save to DB", Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    } catch (PDOException $e) {
        return new Response("unable to save to DB", Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return new Response("form has been submitted");
}

function game_studio() {
    return new Response(
        file_get_contents('dovkolo.html'),
        Response::HTTP_OK,
        array('content-type' => 'text/html')
    );
}