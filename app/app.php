<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 10/1/2016
 * Time: 5:27 PM
 */

require 'Router.php';
require 'actions.php';
require __DIR__.'/../vendor/autoload.php';
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\Yaml\Yaml;

$router = new Router('/beedevs', 'routes.php');
$response = $router->route();
if ($response === false) {
    $response= new Response("Page hasn't been found 404", Response::HTTP_NOT_FOUND);
    send($response);
} else if ($response instanceof Response) {
    send($response);
}

function twig() {
    $loader = new Twig_Loader_Filesystem('view');
    $twig = new Twig_Environment($loader);

    $function = new Twig_SimpleFunction('url', function($route, array $options = array()){
        global $router;
        return $router->url($route, $options);
    }, array('is_variadic' => true));
    $twig->addFunction($function);

    $function = new Twig_SimpleFunction('asset', function($asset){
        global $router;
        return $router->basePath() .  $asset;
    });
    $twig->addFunction($function);

    return $twig;
}

function send($response, $request = null) {
    if ($request === null) {
        $request = Request::createFromGlobals();
    }
    $response->prepare($request);
    $response->send();
}

function params() {
    $params = Yaml::parse(file_get_contents('app/params.yml'));
    return $params;
}

function pdo() {
    $params = params();
    $host = $params['db_host'];
    $name = $params['db_name'];
    return new PDO("mysql:host=$host;dbname=$name;charset=UTF8;", $params['db_user'], $params['db_pass'], array(PDO::ATTR_PERSISTENT=>false));
}