<?php

/**
 * Created by PhpStorm.
 * User: ader
 * Date: 10/1/2016
 * Time: 8:35 PM
 */
class Router {

    private $base_path;

    private $routes;

    public function __construct($base_path, $routes) {
        $this->base_path = $base_path . '/';
        $this->routes = include($routes);
    }

    public function route() {
        $uri = $this->cleanURI($_SERVER['REQUEST_URI']);

        foreach($this->routes as $route => $path){

            if (isset($path['params'])) {
                for ($i = 1; $i <= count($path['params']); $i++) {
                    $path['url'] = str_replace("$$i", $path['params'][$i-1], $path['url']);
                }
            }

            $pattern = '/'. str_replace('/', '\/', $this->base_path . $path['url']) .'/';

            if(preg_match($pattern, $uri)){
                $action_route = preg_replace($pattern, $route, $uri);

                $params = explode('/', $action_route);

                $action = array_shift($params);
                if (is_callable($action)){
                    return call_user_func_array($action, $params);
                }

                return false;
            }
        }

        return false;
    }

    public function url($route_name, $params = null) {

        foreach($this->routes as $route => $path){

            if ($route_name == $route) {

                if (isset($path['params'])) {

                    for ($i = 1; $i <= count($path['params']); $i++) {
                        if (preg_match('/'.$path['params'][$i-1].'/', $params[$i-1])) {
                            $path['url'] = str_replace("$$i", $params[$i-1], $path['url']);
                        }
                    }
                }

                return $this->basePath() . $path['url'];
            }
        }
        return null;
    }

    public function basePath() {
        return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $this->base_path;
    }

    protected function cleanURI($uri) {
        return trim(explode('?', $uri)[0]);
    }

}