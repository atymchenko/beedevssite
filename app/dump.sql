CREATE DATABASE  IF NOT EXISTS `beedevs` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `beedevs`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: beedevs
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'test','adellantado@gmail.com','test'),(2,'etst','test@test.tt','test'),(3,'test','test@test.tt','test'),(4,'test05','test05@a.aa','test05');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `websites`
--

DROP TABLE IF EXISTS `websites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `websites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `name_canonical` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `features` text,
  `about` text,
  `about_short` varchar(255) DEFAULT NULL,
  `small_img` varchar(255) NOT NULL,
  `medium_img` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `websites`
--

LOCK TABLES `websites` WRITE;
/*!40000 ALTER TABLE `websites` DISABLE KEYS */;
INSERT INTO `websites` VALUES (1,'RENAISSANCE RESIDENCE','renaissance_residence','Unique project','Realty','Complex coding of the custom design. Adaptive design for tablets and mobile devices. Developed a method of changing the content without reloading the site. Made changes in GoogleMaps with the placement of the customer\'s objects on it.','Renaissance Residence is a real estate agency and a living complex for a luxury segment of society, offering De Luxe apartments and houses in a wealthy and prestigious neighbourhood in one of the European major cities.','Complex coding of the custom design for Luxury real estate','images/webproj/small/1.png','images/webproj/medium/1.png'),(2,'PHANTOM BOUTIQUE','phantom_boutique','eCommerce','Selling','Developed the custom style of the player for the video uploaded when entering the website. Performed a complex coding of the custom design with animation in it.','“PHANTOM BOUTIQUE” is a luxury clothes brand with a wide range of models and the aim to emphasize the good and exceptional taste. PHANTOM BOUTIQUE uses only natural fabrics such as cashmere, angora, wool, leather and silk produced by the known Italian factories.','Custom web design for clothes web design of luxury Brand','images/webproj/small/2.png','images/webproj/medium/2.png'),(3,'RESTAURANT FELLINY','restaurant_felliny','Unique project','Services','Complex custom design of the website with the animation. Added GoogleMaps location map with the ability of its further self-management. Multilingual site and own news feed.','FELLINI is an Italian restaurant with the variety of gourmet dishes which allows fully enjoy the incredible taste of Italian cuisine.','Discover the world of gourmet dishes with Italian restaurant - Fellini. Creation web design for restaurant.','images/webproj/small/3.png','images/webproj/medium/3.png'),(4,'WELLNESS AND SPA CENTER','wellness_and_spa_center','Unique project','Beauty','Unification of the services site with the cosmetic product catalog. Complex custom design. Functionality for customer registration. Functionality for booking the services and e-commerce products from the site. News feed without dividing the pages, with the automatic loading.','Wellness and Spa center with a variety of pleasant procedures for body and soul, taking care of your health.','Development of Web design of a unique spa and wellness center.','images/webproj/small/4.png','images/webproj/medium/4.png'),(5,'KIEV-STANDARD','kiev_standard','Portal','Realty','Complex coding of the custom website design. Added animation and special effects in the input of the site content. Adding real estate catalog without site reloading.','The atmosphere of a country cottage in the center of the city. The combination of reliability and quality European materials, functional living spaces and private infrastructure.','Creation web design for realty catalogue. Premium class real estate services','images/webproj/small/5.png','images/webproj/medium/5.png'),(6,'HBS COMPANY','hbs_company','Corporate','Production','Individual corporate identity. Multilevel product catalog.','HBS company specializes in the production of the top-class welding machines for miscellaneous purposes of the tech industry.','Creation industrial web design. Welding machines\' production','images/webproj/small/6.png','images/webproj/medium/6.png'),(7,'RIVAGELINE COSMETICS','rivageline_cosmetics','eCommerce','Beauty','Custom website design. Multilevel catalog of cosmetics. Functionality for an easy registration and receiving the orders.','E-commerce site of Rivageline Cosmetics - the natural cosmetics based on the mud from Dead Sea.','Creation and develope webdesign for official website. Cosmetics based on Dead Sea mud and its natural ingredients.','images/webproj/small/7.png','images/webproj/medium/7.png'),(8,'INTECO CONSTRUCTION','inteco_construction','Catalog','Production','Complex product catalog with filters as per individual tech characteristics. Complex coding of the custom website design.','One of the leading manufacturers of metal, metal sheets and sandwich panels in the domestic market of the construction materials','Web developent of industrial sites. It is one of the leading manufacturers of metal','images/webproj/small/8.png','images/webproj/medium/8.png'),(9,'BUSOV HILL','busov_hill','Landing','Realty','Custom design of a landing site. Adding animation and complex site navigation system. Located the customer\'s objects on GoogleMaps. Adaptive design to look beautiful on various tablets and mobile devices.','Real estate agency with the top class apartments located on the hills with the picturesque views.','Developing the site for Busov Hill, we achieved perfection in every detail','images/webproj/small/9.png','images/webproj/medium/9.png'),(10,'CAPITAL ESTATE','capital_estate','Catalog','Realty','Catalog with the automatic loading of real estate objects. Complex custom website design. Functionality to work with databases. Complex search filters. Located the customer\'s objects on GoogleMaps.','Capital Estate offers all-inclusive package of real estate and consulting servcies throughout Europe.','Web development of realty calalog. Universal operator in the overseas property market.','images/webproj/small/10.png','images/webproj/medium/10.png'),(11,'IDGROUP LUXURY ESTATE','idgroup_luxury_estate','Catalog','Realty','Complex coding of the custom website design. Development of the adaptive version of the site. Functionality to work with the large databases, automatic lodaing of objects. Complex search system with filters.','Real estate brokerage & investing company IDGroup Premium specializes on sales of the real estate objects of a premium class.','Develop of realty website webdesign. IDGroup Premium real estate brokerage & investing','images/webproj/small/11.png','images/webproj/medium/11.png'),(12,'SINGER OLEG LYUBIMOV','singer_oleg_lyubimov','Personal','Production','Performing the task to create a website with your music online. A unique design of the site and the type of player. Added spetsefekty and complex. Implemented responsive design for mobile devices','It has long been aware of the fact that the songs can influence a person. Invigorating and fun can even improve health, but there are special songs that can adjust to the romantic mood. Such presents you Oleg Lyubimov.','WebMarker Studio is a creative website design agency specializing in attractively responsive web site design which look professionally and beautifully on any screen','images/webproj/small/12.png','images/webproj/medium/12.png'),(13,'ART PAINTINGS','art_paintings','Personal','Production','The challenge faced develop a nice and quality site with a unique design and user friendly interface. Do not load some extra website design and publish details of all available information.','The site of artist Hope Savchenko. Description and offer services to sitnah artistic painting ceilings and w and on furniture.','The challenge faced develop a nice and quality site with a unique design and user friendly interface.','images/webproj/small/13.png','images/webproj/medium/13.png'),(14,'IT DEPARTMENT','it_department','','','Develop a website to inform students of OT FIVT questions about training, post interesting information and news at the department. Post a catalog of problems and benefits.','Personal site lecturer OT FIVT NTU KPI. Experimental platform to help teachers and Countdown creating communication with students.','Website development and design, made personal site layout PSD teacher NTU KPI','images/webproj/small/14.png','images/webproj/medium/14.png'),(15,'GEARSHOP','gearshop','Personal','Production','Develop online store with sophisticated functionality and filtering system parameters. Working with a large number of products, development of complex database online store. Provide the ability to create and payment orders through the online store','Internet - GearShop store specializing in the sale of products and accessories for motorcycles and ATVs.','A shop goods and accessories for motorcycles. Advanced Shop','images/webproj/small/15.png','images/webproj/medium/15.png'),(16,'SEPTIKVDOM','septikvdom','Personal','Production','Create odnostornikovyh - Landing site for the supply and installation services for interested septic tank, kanalihatsiyi through new technologies.','Company \"Septic in house\" provides a quality setting for sewage latest technology.','Created Landing - page ad type of company offers installation of sewerage systems of new generation','images/webproj/small/16.png','images/webproj/medium/16.png'),(17,'KIEV PROPERTY EXPO-FORUM','kiev_property_expo_forum','Unique project','Realty','The uniqueness of the project was to develop a reservation system the stand area in exhibition-forum with the ability of management and communication with clients. Developed unique design and content management dobalennyam Forum partners and countries involved.','Exhibition - Forum estate KIEV PROPERTY EXPO-FORUM is one of the largest real estate exhibition in Kiev. The exhibition offers the latest proposals from developers and objects as Ukrainian and foreign property','WebMarker Studio is a creative website design agency specializing in attractively responsive web site design which look professionally and beautifully on any screen','images/webproj/small/17.png','images/webproj/medium/17.png'),(18,'COMPANY SCROOGE CM','company_scrooge_cm','Personal','Production','','','WebMarker Studio is a creative website design agency specializing in attractively responsive web site design which look professionally and beautifully on any screen','images/webproj/small/18.png','images/webproj/medium/18.png'),(19,'AF AUDIT KIEV','af_audit_kiev','Personal','Production','','','WebMarker Studio is a creative website design agency specializing in attractively responsive web site design which look professionally and beautifully on any screen','images/webproj/small/19.png','images/webproj/medium/19.png'),(20,'YOGA CENTER YOGA LIFE','yoga-center_yoga_life','Personal','Production','','','WebMarker Studio is a creative website design agency specializing in attractively responsive web site design which look professionally and beautifully on any screen','images/webproj/small/20.png','images/webproj/medium/20.png'),(21,'PORTAL UA TAXI','portal_ua_taxi','Personal','Production','','','WebMarker Studio is a creative website design agency specializing in attractively responsive web site design which look professionally and beautifully on any screen','images/webproj/small/21.png','images/webproj/medium/21.png'),(22,'WEDDING AGENCY','wedding_agency','Personal','Production','','','WebMarker Studio is a creative website design agency specializing in attractively responsive web site design which look professionally and beautifully on any screen','images/webproj/small/22.png','images/webproj/medium/22.png'),(23,'COMPANY CLEARAIR','company_clearair','Personal','Production','','','WebMarker Studio is a creative website design agency specializing in attractively responsive web site design which look professionally and beautifully on any screen','images/webproj/small/23.png','images/webproj/medium/23.png'),(24,'MASTERS COMPANY','masters_company','Personal','Production','','','WebMarker Studio is a creative website design agency specializing in attractively responsive web site design which look professionally and beautifully on any screen','images/webproj/small/24.png','images/webproj/medium/24.png'),(25,'COMPANY DOMINANT','company_dominant','Personal','Production','','','WebMarker Studio is a creative website design agency specializing in attractively responsive web site design which look professionally and beautifully on any screen','images/webproj/small/25.png','images/webproj/medium/25.png'),(26,'MAINE COON CATS','maine_coon_cats','Personal','Production','','','WebMarker Studio is a creative website design agency specializing in attractively responsive web site design which look professionally and beautifully on any screen','images/webproj/small/26.png','images/webproj/medium/26.png'),(27,'PORTAL ARTHAUS STUDIO','portal_arthaus_studio','Personal','Production','','','WebMarker Studio is a creative website design agency specializing in attractively responsive web site design which look professionally and beautifully on any screen','images/webproj/small/27.png','images/webproj/medium/27.png'),(28,'COMPANY FRAMEHOUSE','company_framehouse','Personal','Production','','','WebMarker Studio is a creative website design agency specializing in attractively responsive web site design which look professionally and beautifully on any screen','images/webproj/small/28.png','images/webproj/medium/28.png'),(29,'LCD ZVERINETSKY','lcd_zverinetsky','Personal','Production','','','WebMarker Studio is a creative website design agency specializing in attractively responsive web site design which look professionally and beautifully on any screen','images/webproj/small/29.png','images/webproj/medium/29.png'),(30,'MORAY DIVING CENTRE','moray_diving_centre','Personal','Production','','','WebMarker Studio is a creative website design agency specializing in attractively responsive web site design which look professionally and beautifully on any screen','images/webproj/small/30.jpg','images/webproj/medium/30.jpg');
/*!40000 ALTER TABLE `websites` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-02 17:07:03
