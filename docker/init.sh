#!/bin/sh

home="/var/www/html/beedevssite"
service mysql start
mysql -p12345 < "$home"/app/dump.sql
composer install --working-dir="$home"
/usr/bin/supervisord -c /home/bee/supervisord.conf
